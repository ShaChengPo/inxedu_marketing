<div class="mbqh-qt" style="display: none">
	<ul>
		<li id="page7" class="li-1">
			<a href="javascript:void (0)"  onclick="setHelloPage(7)" class="f-fM " title="简约风">经典版</a>
		</li>
		<li id="page47" class="li-2">
			<a href="javascript:void (0)"  onclick="setHelloPage(47)" class="f-fM " title="小清新">简约风</a>
		</li>
		<li id="page48" class="li-3">
			<a href="javascript:void (0)"  onclick="setHelloPage(48)" class="f-fM " title="经典版">小清新</a>
		</li>
		<li id="page49" class="li-4">
			<a href="javascript:void (0)"  onclick="setHelloPage(49)" class="f-fM " title="创意秀">时尚感</a>
		</li>
		<li id="page1" class="li-5">
			<a href="javascript:void (0)"  onclick="setHelloPage(1)" class="f-fM " title="科技风">科技风</a>
		</li>
		<li id="page2" class="li-6">
			<a href="javascript:void (0)"   onclick="setHelloPage(2)" class="f-fM " title="时尚感">致青春</a>
		</li>
		<li id="page3" class="li-7">
			<a href="javascript:void (0)" onclick="setHelloPage(3)" class="f-fM " title="致青春">创意秀</a>
		</li>
	</ul>
</div>
<style>
    #message{width: 260px;height: 48px;position: absolute;left: 50%;z-index: 999;}
    .msg-nr{border-radius: 6px;overflow: hidden;border: 1px solid #d6d6d6;background: #fff;text-align: center;}
    .msg-nr span{font-size: 14px;line-height: 48px;text-align: center;margin: 0;display: inline-block;}
    .msg-nr .rts-ico{display: inline-block;vertical-align: middle;width: 20px;height: 20px;margin-right: 5px;}
    .msg-nr .rts-ico img{display: block;width: 100%;height: 100%;}
</style>
<script>
	$(function(){
		if (self ==top){
            var url=window.location.pathname;

            //if(url=="/"||url=="/index.html"){
                $(".mbqh-qt").show()
            //}
		}
		/*切换页面按钮选中添加状态*/
        var pageId = $("#selectPageBtn").val();
        var id = "#page"+pageId
        $(id).addClass("current")
	})
    /**
     * 弹出框 （弱提示）
     * @param info
     */
    function msgshow(info,success){
        $("#message").remove();
        $(".mes-warp-0").remove();
        var msgBox = $('<div id="message"></div>').appendTo($("body")).fadeIn("fast").delay(3000).fadeOut("slow");
        /*var msgTxt = [
            '<div class="mes-warp-0">'+
            '<div class="msg-cg msg-nr"><p>恭喜你修改成功！</p></div>'+
            '</div>'
            ,
            '<div class="mes-warp-1">'+
            '<div class="msg-sb msg-nr"><p>修改失败！</p></div>'+
            '</div>'
        ];*/
        if (success=="false"){
            var msgTxt =
                    '<div class="mes-warp-0">'+
                    '<div class="msg-cg msg-nr msg-nr-cw"><em class="rts-ico"><img src="/static/admin/assets/rts-cw.png"></em><span>'+info+'</span></div>'+
                    '</div>'
        }else{
            var msgTxt =
                    '<div class="mes-warp-0">'+
                    '<div class="msg-cg msg-nr msg-nr-zq"><em class="rts-ico"><img src="/static/admin/assets/rts-zq.png"></em><span>'+info+'</span></div>'+
                    '</div>'
        }


        $("#message").html(msgTxt);
        var dTop = (parseInt(document.documentElement.clientHeight, 10)/2) + (parseInt(document.documentElement.scrollTop || document.body.scrollTop, 10)),
                dH = msgBox.height(),
                dW = msgBox.width(),
                timer = null,
                dClose;
        msgBox.css({"top" : (dTop-(dH/2)) , "margin-left" : -(dW/2)});
        dClose = function() {msgBox.remove();};
    }
	/*使用模板 设为首页*/
	function setHelloPage(webpageId) {
		$.ajax({
			url:'/admin/ajax/setHelloPage/'+webpageId,
			type:'post',
			dataType:'json',
            beforeSend:function(){
                var i = 5;
                msgshow('正在更换页面请稍等！'+i);
                var count = setInterval(function () {
                    if (i>1){
                        msgshow('正在更换页面请稍等！'+parseInt(i-1));
                        i--
                    }else{
                        i=5;
                        clearInterval(count);
                        //重新加载
                        window.location.reload();

                        //重新加载页面 和 左侧菜单
                        //getWebpageTemplates('/index.html',null);
                    }
                },900);

                //模板 是否 修改 過(排序 ，刪除 ，新增)
                isDataChange=false;//還原后默認false

            },
			success:function(result){
				if(result.success==true){
					/*var i = 3;
					var count = setInterval(function () {
						if (i>0){
                            msgshow(result.message+i);
							i--
						}else{
							i=3;
							clearInterval(count);
							//重新加载
							window.location.reload();

							//重新加载页面 和 左侧菜单
							//getWebpageTemplates('/index.html',null);
						}
					},600);

					//模板 是否 修改 過(排序 ，刪除 ，新增)
					isDataChange=false;//還原后默認false*/

				}else{
					msgshow(result.message);
				}
			},
			error:function(error){
				msgshow("系统繁忙，请稍后再操作！");
			}
		});
	}
</script>