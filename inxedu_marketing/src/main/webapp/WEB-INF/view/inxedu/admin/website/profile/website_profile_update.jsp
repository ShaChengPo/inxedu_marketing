<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>网站配置管理</title>
	<link rel="stylesheet" type="text/css" href="${ctximg}/kindeditor/themes/default/default.css" />
	<script type="text/javascript" src="${ctximg}/kindeditor/kindeditor-all.js"></script>
	<script type="text/javascript">
		var type='';
		$(document).ready(function() {
			type='${type}';
			$("#"+type).attr("href","javascript:void(0)");
			if(type=='web'){
				initSimpleImageUpload('imguploadButton','QRcode',callback);
				initSimpleImageUpload('fileuploadButton','websiteLogo',callbacklogo);
			}
		});
		function callbacklogo(imgUrl){
			$('#logopic').attr("src",'<%=staticImage%>'+imgUrl);
			$("#logopic").show();
			$('#imagesUrl').val(imgUrl);
		}
		function callback(imgUrl){
			$('#imgsubjcetpic').attr("src",'<%=staticImage%>'+imgUrl);
			$("#imgsubjcetpic").show();

			$('#imgUrl').val($("#imgsubjcetpic").attr("src").replace('${ctx}',""));
		}

		function formSubmit(){
			if(type!=''){
				if(type=='web'){
					var icoFile=$("#icoFile").val();
					if(icoFile!=null&&icoFile!=''){
						var fileNames=icoFile.split('.');
						var fileName=fileNames[0];
						var fileNameSuffix=fileNames[1];
						if(!(fileNameSuffix == "ico" || fileNameSuffix == "ICO")) {
							msgshow("请选择ico格式的图片!",'false');
							return;
						}else if(fileName.indexOf('favicon')==-1)
						{
							msgshow("文件必须命名为favicon",'false');
							return;
						}
					}
				}
				$("#addprofileForm").submit();
			}
		}

	</script>
</head>
<body>
<div class="rMain">
	<form action="${ctx }/admin/websiteProfile/update" method="post" id="addprofileForm" enctype="multipart/form-data">
		<input type="hidden" name="type" value="${type}" />
		<fieldset>
			<div class="mt20">
				<p>
					<label ><span>
							<tt class="c_666 ml20 fsize12">
								（<font color="red">*</font>&nbsp;为必填项）
							</tt>
						</span></label>
					<span class="field_desc"></span>
				</p>
				<c:if test="${type=='web' }">
					<p>
						<label ><font color="red">*</font>&nbsp;网站title</label>
						<input type="text" name="title" value="${webSiteMap.web.title}" class="lf" />
						<span class="field_desc">(网站头部)</span>
					</p>
					<p>
						<label ><font color="red">*</font>&nbsp;网校名称</label>
						<input type="text" name="company" value="${webSiteMap.web.company}" class="lf" />
						<span class="field_desc">(网站头部)</span>
					</p>
					<p>
						<label ><font color="red">*</font>&nbsp;网站作者</label>
						<input type="text" name="author" value="${webSiteMap.web.author}" class="lf" />
						<span class="field_desc"></span>
					</p>
					<p>
						<label ><font color="red">*</font>&nbsp;关键词</label>
						<input type="text" name="keywords" value="${webSiteMap.web.keywords}" style="width: 75%" class="lf" />
						<span class="field_desc"></span>
					</p>
					<p>
						<label ><font color="red">*</font>&nbsp;描述</label>
						<input type="text" name="description" value="${webSiteMap.web.description}" style="width: 75%" class="lf" />
						<span class="field_desc"></span>
					</p>
					<p>
						<label ><font color="red">*</font>&nbsp;联系邮箱</label>
						<input type="text" name="email" value="${webSiteMap.web.email}" class="lf" />
						<span class="field_desc"></span>
					</p>
					<p>
						<label ><font color="red">*</font>&nbsp;24小时客服服务热线</label>
						<input type="text" name="phone" value="${webSiteMap.web.phone}" class="lf" />
						<span class="field_desc"></span>
					</p>
					<p>
						<label ><font color="red">*</font>&nbsp;工作时间</label>
						<input type="text" name="workTime" value="${webSiteMap.web.workTime}" class="lf" />
						<span class="field_desc"></span>
					</p>
					<p>
						<label ><font color="red">*</font>&nbsp;地址</label>
						<input type="text" name="location" value="${webSiteMap.web.location}" class="lf" />
						<span class="field_desc"></span>
					</p>
					<p>
						<label ><font color="red">*</font>&nbsp;版权以及备案号</label>
						<input type="text" name="copyright" value="${webSiteMap.web.copyright}" style="width: 75%" class="lf" />
						<span class="field_desc">(网站底部)</span>
					</p>
					<p>
						<label ><font color="red">*</font>&nbsp;ico文件</label>
						<img class="icoimg" alt="" src="${ctx}/favicon.ico?v=<%=Math.random()*100%>">
						<input type="file" name="icoFile" id="icoFile"  class="no-bor"/>
						<span class="field_desc"></span>
						<font color="red">（请美工制作图片的大小标为32*32的ico图片,否则图片会失真）</font>
					</p>
					<p>
						<label ><font color="red">*</font>&nbsp;网站logo</label>
						<input type="hidden" name="logoUrl" id="imagesUrl" value="${webSiteMap.web.logoUrl}" style="width: 450px;" />
						<img alt="" src="<%=staticImage%>${webSiteMap.web.logoUrl}" id="logopic" width="144px" height="90px" />
						<input type="button" id="fileuploadButton" value="上传" />
						<span class="field_desc"><font color="red">*LOGO链接，支持(长236*宽72)像素JPG、PNG格式</font></span>
					</p>

					<p>
						<label ><font color="red">*</font>&nbsp;网站底部二维码</label>
						<input type="hidden" name="imgUrl" id="imgUrl" value="${webSiteMap.web.imgUrl}" style="width: 450px;" />
						<img alt="" src="<%=staticImage%>${webSiteMap.web.imgUrl}" id="imgsubjcetpic" width="144px" height="90px" />
						<input type="button" id="imguploadButton" value="上传" />
						<span class="field_desc"><font color="red">*底部二维码，支持(长150*宽150)像素JPG、PNG格式</font></span>
					</p>

					</tbody>
				</c:if>
				<c:if test="${type=='censusCode'}">
					<p>
						<label ><font color="red">*</font>&nbsp;统计代码</label>
						<textarea rows="6" cols="60" name="censusCodeString">${webSiteMap.censusCode.censusCodeString}</textarea>
						<span class="field_desc"></span>
					</p>
				</c:if>
				<p>
					<input class="button" type="button" value="保存" onclick="formSubmit()"/>
					<input class="button" type="button" value="返回" onclick="history.go(-1)" />
				</p>
			</div>
			<!-- /tab4 end -->
		</fieldset>
	</form>
</div>
</body>
</html>
