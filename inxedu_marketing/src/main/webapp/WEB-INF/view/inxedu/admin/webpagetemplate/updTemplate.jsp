<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <!-- Meta -->
    <meta charset="utf-8" http-equiv="Content-Type" />
    <!-- End of Meta -->
    <!-- Page title -->
    <title>修改模板内容${websitemap.web.company}-${websitemap.web.title}</title>
    <!-- End of Page title -->
    <meta name="author" content="${websitemap.web.author}" />
    <meta name="keywords" content="${websitemap.web.keywords}" />
    <meta name="description" content="${websitemap.web.description}" />
    <link rel="shortcut icon" href="${ctx}/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/template/templet1/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/template/templet1/css/web.css">
    <link rel="stylesheet" type="text/css" href="/template/templet1/css/ie.css">
    <link rel="stylesheet" type="text/css" href="/template/templet1/css/mislider.css">
    <link rel="stylesheet" type="text/css" href="/template/templet1/css/mislider-skin-cameo.css">
    <script src="/template/templet1/js/modernizr-custom.js"></script>
    <script src="/template/templet1/js/common.js" type="text/javascript"></script>
    <script src="/template/templet1/js/swiper-2.1.0.js" type="text/javascript"></script>
    <script src="/template/templet1/js/mislider.js"></script>
    <link href="/template/templet1/css/mw_320_768.css" rel="stylesheet" type="text/css" media="screen and (min-width: 320px) and (max-width: 768px)">
    <link rel="stylesheet" type="text/css" href="/template/templet1/css/color.css">
    <!--[if lt IE 9]><script src="/template/templet1/js/html5.js"></script><![endif]-->

    <link rel="stylesheet" type="text/css" href="${ctx}/kindeditor/themes/default/default.css" />
    <script type="text/javascript" src="${ctx}/kindeditor/kindeditor-all.js"></script>

    <script>
        //上传图片描述
        var upload_image_info="${upload_image_info}";
        //是否上传图片
        var upload_image="${upload_image}";
        //是否新增图片
        var add_new_image="${add_new_image}";
        //是否删除
        var delete_data="${delete_data}";
        var show_moreOneInfo ="${show_moreOneInfo}";
        //是否显示标题
        var show_title="${show_title}";
        /*显示五行模板信息*/
        var show_moreFiveTempInfo="${show_moreFiveTempInfo}";
        //是否显示详情
        var show_info="${show_info}";
        //是否显示图片广告图点击链接
        var show_image_link="${show_image_link}";
        //是否调用接口的动态数据
        var dynamic_data="${dynamic_data}";
        //是否显示备用描述
        var show_infoStandby = "${show_infoStandby}";
        //是否显示备用标题
        var show_titleStandby = "${show_titleStandby}";
        /*是否显示教师简介*/
        var show_intruduction = "${show_intruduction}";
        /* 是否显示首页关于我们内容*/
        var show_aboutContent = "${show_aboutContent}";
        /*判断是否是banner图*/
        var is_banner = "${is_banner}";
        /*多显示2行描述*/
        var show_moreTwoInfo ="${show_moreTwoInfo}";
        //是否显示首页关于我们内容
        var show_aboutContent_style="";
        if(show_aboutContent=="false"){
            show_aboutContent_style='style="display: none"';
        }
        //是否上传图片(用于js 新增 显示隐藏)
        var upload_image_style="";
        if(upload_image=="false"){
            upload_image_style='style="display: none"';
        }
        //是否删除(用于js 新增 显示隐藏)
        var delete_data_style="";
        if(delete_data=="false"){
            delete_data_style='style="display: none"';
        }
        //是否显示标题(用于js 新增 显示隐藏)
        var show_title_style="";
        if(show_title=="false"){
            show_title_style='style="display: none"';
        }
        //是否显示详情(用于js 新增 显示隐藏)
        var show_info_style="";
        if(show_info=="false"){
            show_info_style='style="display: none"';
        }
        //是否显示图片广告图点击链接(用于js 新增 显示隐藏)
        var show_image_link_style="";
        if(show_image_link=="false"){
            show_image_link_style='style="display: none"';
        }
        //是否显示备用标题
        var show_titleStandby_style="";
        if(show_titleStandby=="false"){
            show_titleStandby_style='style="display: none"';
        }
        //是否显示教师简介
        var show_intruduction_style="";
        if(show_intruduction=="false"){
            show_intruduction_style='style="display: none"';
        }

        /**
         * 提交信息
         */
        function saveImage(){
            /*备用描述拼接描述字符串*/
           /* var size = $("#templateData li").size();
            for (var i=0;i<size;i++){
                $("#describe"+i).val($("#describe"+i).val()+"/"+$("#describeTwo"+i).val()+"/"+$("#describeThree"+i).val());
            }*/

            /*显示教师简介时拼接标符串*/
            if (show_intruduction=="true"){
                var size = $("#templateData li").size();
                for (var i=0;i<size;i++){
                    $("#describe"+i).val($("#intruduction"+i).val()+"/"+$("#describe"+i).val())
                }
            }
            /*显示首页关于我们内容时拼接字符串*/
            if (show_aboutContent=="true"){
                $("#templateInfo1").val($("#templateInfo1").val()+"/"+$("#aboutContent").val());
            }
            if (show_moreFiveTempInfo=="true"){
                $("#templateInfo1").val($("#templateInfo1").val()+"/"+$("#aboutContentOne").val()+
                        "/"+$("#aboutContentTwo").val()+"/"+$("#aboutContentThree").val()+"/"+$("#aboutContentFour").val()+"/"+$("#aboutContentFive").val())
            }
            /*显示备用标题时拼接标题字符串*/
            if (show_titleStandby=="true"){
                if ($("#name1").val()==""||$("#name2").val()==""){
                    msgshow("请填写标题！");
                    return;
                }
                if ($("#templateInfo1").val()==""||$("#templateInfo2").val()==""){
                    msgshow("请填写英文！");
                    return;
                }
                $("#name1").val($("#name1").val()+"/"+$("#name2").val());
                $("#templateInfo1").val($("#templateInfo1").val()+"/"+$("#templateInfo2").val());
                var size = $("#templateData li").size();
                for (var i=0;i<size;i++){
                $("#describe"+i).val($("#describe"+i).val()+"/"+$("#comfrom"+i).val());
                }
            }
            //$("#saveImagesForm").submit();

            if("${show_multi_data_remark}">0){
                $("#templateData").find("table").each(function(indextr,tagtable){
                    //拼接后的字符串
                    var show_multi_data_remark_str="";
                    //每个描述的下标
                    var describeIndex="";
                    $(tagtable).find("textarea[name='show_multi_data_remark-textarea']").each(function(index,tag){
                        show_multi_data_remark_str+=isNotEmpty($(tag).val())?$(tag).val():" ";
                        if(index+1<"${show_multi_data_remark}"){//判断不是最后最后一个
                            show_multi_data_remark_str+="|";
                        }
                        describeIndex=$(tag).attr("describeIndex");
                    });
                    $("textarea[name='WebsiteImagesList["+describeIndex+"].describe']").val(show_multi_data_remark_str);
                });
            }
            $.ajax({
                url:baselocation+'/admin/WebsiteImages/updWebsiteImagesList',
                type:'post',
                data:$("#saveImagesForm").serialize(),
                async:false,
                dataType:'json',
                success:function(result){
                    if(result.success){
                        msgshow("修改成功");
                        $('#previewFrame').attr('src', $('#previewFrame').attr('src'));
                        window.location.reload()
                    }
                    else{
                        msgshow("修改失败");
                    }
                }
            })
        }
        /*显示教师简介时拼接标题字符串*/
        $(function () {
            if (show_intruduction=="true"){
                var size = $("#templateData li").size();
                for (var i=0;i<size;i++){
                    var describe = [];
                    var describeInfo = $("#describe"+i).val();
                    describe = describeInfo.split("/");
                    $("#intruduction"+i).val(describe[0]);
                    $("#describe"+i).val(describe[1]);
                }
            }
        });
        $(function () {
            if (show_moreFiveTempInfo=="true"){
                var templateInfoArr = [];
                templateInfoArr = $("#templateInfo1").val().split("/");
                $("#templateInfo1").val(templateInfoArr[0]);
                $("#aboutContentOne").val(templateInfoArr[1]);
                $("#aboutContentTwo").val(templateInfoArr[2]);
                $("#aboutContentThree").val(templateInfoArr[3]);
                $("#aboutContentFour").val(templateInfoArr[4]);
                $("#aboutContentFive").val(templateInfoArr[5]);
            }
        });
        /* 关于我们内容添加*/
        $(function () {
            if (show_aboutContent=="true"){
                var templateInfo1 = $("#templateInfo1").val();
                var templateInfo1Arr = [];
                templateInfo1Arr = templateInfo1.split("/");
                $("#templateInfo1").val(templateInfo1Arr[0]);
                $("#aboutContent").val(templateInfo1Arr[1]);
            }
        });
        /*显示备用标题时拼接标题字符串*/
        $(function () {
            if (show_titleStandby=="true"){
                var title = $("#name1").val();
                var titleArr = [];
                titleArr = title.split("/");
                $("#name1").val(titleArr[0]);
                $("#name2").val(titleArr[1]);
                var templateInfo = $("#templateInfo1").val();
                var templateInfoArr = [];
                templateInfoArr = templateInfo.split("/");
                $("#templateInfo1").val(templateInfoArr[0]);
                $("#templateInfo2").val(templateInfoArr[1]);

            }
        });

        /**
         * 新增 内容
         */
        function addImage() {
            var index=$("table").size();

            //获取最大的排序值
            var maxSort=$('#templateData li:last').find(".hiddenSort").val();
            if(maxSort==undefined){
                maxSort=0;
            }

            var show_multi_data_remark_html="";
            if (parseInt("${show_multi_data_remark}")>0){
                for(var i=0;i<parseInt("${show_multi_data_remark}");i++){
                    show_multi_data_remark_html+=
                            '<tr>'
                            +'    <td style="text-align: left;">'
                            +'            <span class="tit-name"> <font color="red">*</font>描述'+(i+1)+'：</span>'
                            +'    <textarea name="show_multi_data_remark-textarea" describeIndex="'+index+'" class="txt"></textarea>'
                            +'            </td>'
                            +'            </tr>';
                }
            }

            var htmlStr='<li>'
                    +'<%--<input type="hidden" name="WebsiteImagesList[${index.index}].imageId" value="${websiteImages.imageId}">--%>'
                    +'   <input type="hidden" name="WebsiteImagesList['+index+'].imagesUrl" id="imagesUrlInput'+index+'" value="">'
                    +'       <input type="hidden" name="WebsiteImagesList['+index+'].previewUrl" value="">'
                    +'       <input type="hidden" name="WebsiteImagesList['+index+'].webpageTemplateId" value="${templateId}">'
                    +'        <table class="mk-bj-list"  width="50%">'
                    +'       <tr '+upload_image_style+'>'
                    +'        <td style="text-align: left;"  colspan="2">'
                    +'        <span class="tit-name"><font color="red">*</font>图片：</span>'
                    +'       <img id="imagesUrl'+index+'" class="mk-list-img"  src="${ctx }/static/admin/assets/logo.png">'
                    +'       <input type="button" value="上传" id="imageFile'+index+'" class="imageFileUpload"/>'
                    +'       <font color="red">('+upload_image_info+')</font>'
                    +'</td>'
                    +'</tr>'
                    +'<tr>'
                    +'<td '+show_title_style+' style="text-align: left;" width="50%" >'
                    +'        <span class="tit-name"><font color="red">*</font>标题：</span>'
                    +'<input name="WebsiteImagesList['+index+'].title" type="text" value="" class="txt" />'
                    +'        </td>'
                    +'<td style="text-align: left;" width="50%">'
                    +'<span class="tit-name"> <font color="red">&nbsp;</font>排序：</span>'
                    +'       <input id="sort" name="WebsiteImagesList['+index+'].seriesNumber" data-rule="required;" class="txt hiddenSort" value="'+(parseInt(maxSort)+1)+'" onkeyup="this.value=this.value.replace(/\D/g,\'\')" type="hidden">'
                    +'          <a class="acd-ico-btn a-i-b-up" href="javascript:void(0)" onclick="moveUp(this)" title="向上一级">&nbsp;</a>'
                    +'          <a class="acd-ico-btn a-i-b-dwon" href="javascript:void(0)" onclick="moveDown(this)" title="向下一级">&nbsp;</a>'
                    +'          <a class="acd-ico-btn a-i-b-close" href="javascript:void(0)" onclick="$(this).parents(\'li\').remove()" title="删除">&nbsp;</a>'
                    +'          </td>'
                    +'         </tr>'
                    +'        <tr >'
                    +'        <td '+show_intruduction_style+' style="text-align: left;">'
                    +'        <span class="tit-name"> <font color="red">*</font>简介：</span>'
                    +'              <input type="text" id="intruduction'+index+'"  class="txt" />'
                    +'        </td>'
                    +'        <td '+show_image_link_style+' style="text-align: left;">'
                    +'        <span class="tit-name"><font color="red">&nbsp;</font>跳转链接：</span>'
                    +'<input name="WebsiteImagesList['+index+'].linkAddress" type="text" value="" class="txt" />'
                    +'        </td>'
                    +'        </tr>'
                    +'        <tr >'
                    +'        <td '+show_info_style+'style="text-align: left;">'
                    +'        <span class="tit-name"> <font color="red">*</font>描述：</span>'
                    +'              <textarea id="describe'+index+'" name="WebsiteImagesList['+index+'].describe"  class="txt"></textarea>'
                    +'        </td>'
                    +       '</tr>'

                    +show_multi_data_remark_html
                    +'        </table>'
                    +'        </li>';
            $("#templateData").append(htmlStr);

            //初始化上传按钮
            initSimpleImageUpload("imageFile"+index,"template_data_image",imgCallback,"",index);

        }

        /**
         * 设置iframe 高度
         */
        function setIframeHeight() {
            var iframe=document.getElementById('indexFrame');
            if (iframe) {
                var iframeWin = iframe.contentWindow || iframe.contentDocument.parentWindow;
                if (iframeWin.document.body) {
                    iframe.height = iframeWin.document.documentElement.scrollHeight+10 || iframeWin.document.body.scrollHeight+10;
                    $("#indexFrame").css("visibility","visible");
                }
            }
            var previewFrame=document.getElementById('previewFrame');
            if (previewFrame) {
                var iframeWin = previewFrame.contentWindow || previewFrame.contentDocument.parentWindow;
                if (iframeWin.document.body) {
                    previewFrame.height = iframeWin.document.documentElement.scrollHeight+10 || iframeWin.document.body.scrollHeight+10;
                    $("#previewFrame").css("visibility","visible");
                }
            }
        }
        window.onload = function () {
            setIframeHeight();
            setPreviewIframeHeight(document.getElementById('previewFrame'));
        };
        function returnPage() {
            var pageUrl = '${pageUrl}';
            window.parent.parent.getWebpageTemplates(pageUrl,this)
        }

        /**
         * 设置iframe 高度
         */
        function setPreviewIframeHeight(iframe) {
            if (iframe) {
                var iframeWin = iframe.contentWindow || iframe.contentDocument.parentWindow;
                if (iframeWin.document.body) {
                    //iframe.height = iframeWin.document.documentElement.scrollHeight+10 || iframeWin.document.body.scrollHeight+10;
                }
            }
        }
    </script>
</head>
<body>
    <div class="rMain">
        <form action="${ctx}/admin/WebsiteImages/updWebsiteImagesList" method="post" id="saveImagesForm">
            <input type="hidden" name="webpageTemplateId" value="${templateId}">
            <div class="" id="tmplateInfo">
                <p class="hLh30 fsize20 c-333 f-fH">模块预览</p>
                <div class="mt20 mkyl-box">
                    <iframe id="previewFrame" <c:if test="${dynamic_data=='true' && not empty redirectIframeUrl}">style="display: none"</c:if>name="previewFrame" src="${ctx}/admin/webpageTemplate/getContentById/${templateId}" style="overflow:visible;visibility: hidden;" scrolling="yes" frameborder="no" width="100%" height=""></iframe>
                </div>
                <p class="hLh30 fsize20 c-333 f-fH mt30">内容编辑
                    <tt class=" ml20 fsize16">
                        （<font color="red">*</font>&nbsp;为必填项）
                    </tt>
                    <span class="field_desc"></span>
                </p>
                <table class="pic-mb-tab"  width="100%">
                    <tr style="display: none">
                        <td width="10%">
                            <font color="red">*</font>模块名称
                        </td>
                        <td style="text-align: left;">
                            <input name="webpageTemplate.templateName" type="text" value="${webpageTemplate.templateName}" style="width: 580px;" />
                            （后台显示）
                        </td>
                    </tr>
                    <c:if test="${is_banner == false}">
                        <tr>
                            <td width="10%">
                                <font color="red">&nbsp;</font>标题
                            </td>
                            <td style="text-align: left;">
                                <input id="name1" name="webpageTemplate.templateTitle" type="text" value="${webpageTemplate.templateTitle}" style="width: 580px;" />
                            </td>
                        </tr>
                        <tr <c:if test="${show_titleStandby=='false'}">style="display: none"</c:if>>
                            <td width="10%">
                                <font color="red">&nbsp;</font>标题二
                            </td>
                            <td style="text-align: left;">
                                <input id="name2" type="text" value="${webpageTemplate.templateTitle}" style="width: 580px;" />
                            </td>
                        </tr>
                        <tr <c:if test="${not empty redirectIframeUrl}">style="display: none"</c:if>>
                            <td>
                                <font color="red">&nbsp;</font>英文
                            </td>
                            <td style="text-align: left;">
                                <input id="templateInfo1" name="webpageTemplate.info" type="text"  maxlength="600" value="${webpageTemplate.info }" style="width: 580px;"/>
                            </td>
                        </tr>
                    </c:if>
                    <tr class="ifArticle" <c:if test="${show_titleStandby=='false'||not empty redirectIframeUrl}">style="display: none"</c:if>>
                        <td>
                            <font color="red">&nbsp;</font>英文二
                        </td>
                        <td style="text-align: left;">
                            <input id="templateInfo2"  type="text"  maxlength="600" value="${webpageTemplate.info }" style="width: 580px;"/>
                        </td>
                    </tr>
                    <tr <c:if test="${show_aboutContent=='false'}">style="display: none"</c:if>>
                        <td >
                            <font color="red">&nbsp;</font>内容
                        </td>
                        <td style="text-align: left;">
                            <input id="aboutContent"  type="text"  maxlength="600"  style="width: 580px;"/>
                        </td>
                    </tr>
                    <tr <c:if test="${show_moreFiveTempInfo=='false'}">style="display: none"</c:if>>
                        <td >
                            <font color="red">&nbsp;</font>内容一
                        </td>
                        <td style="text-align: left;">
                            <input id="aboutContentOne"  type="text"  maxlength="600"  style="width: 580px;"/>
                        </td>
                    </tr>
                    <tr <c:if test="${show_moreFiveTempInfo=='false'}">style="display: none"</c:if>>
                        <td >
                            <font color="red">&nbsp;</font>内容二
                        </td>
                        <td style="text-align: left;">
                            <input id="aboutContentTwo"  type="text"  maxlength="600"  style="width: 580px;"/>
                        </td>
                    </tr>
                    <tr <c:if test="${show_moreFiveTempInfo=='false'}">style="display: none"</c:if>>
                        <td >
                            <font color="red">&nbsp;</font>内容三
                        </td>
                        <td style="text-align: left;">
                            <input id="aboutContentThree"  type="text"  maxlength="600"  style="width: 580px;"/>
                        </td>
                    </tr>
                    <tr <c:if test="${show_moreFiveTempInfo=='false'}">style="display: none"</c:if>>
                        <td >
                            <font color="red">&nbsp;</font>内容四
                        </td>
                        <td style="text-align: left;">
                            <input id="aboutContentFour"  type="text"  maxlength="600"  style="width: 580px;"/>
                        </td>
                    </tr>
                    <tr <c:if test="${show_moreFiveTempInfo=='false'}">style="display: none"</c:if>>
                        <td >
                            <font color="red">&nbsp;</font>内容五
                        </td>
                        <td style="text-align: left;">
                            <input id="aboutContentFive"  type="text"  maxlength="600"  style="width: 580px;"/>
                        </td>
                    </tr>
                </table>
            </div>
            <c:if test="${not empty websiteImagesList &&dynamic_data=='false'}">
                     <div class="mt30">
                      <%--  <p class="hLh30 fsize20 c-333 f-fH">模块内容编辑
                            <tt class=" ml20 fsize16">
                                （<font color="red">*</font>&nbsp;为必填项）
                            </tt>
                            <span class="field_desc"></span>
                        </p>--%>
                        <ul class="mk-bj-li" id="templateData">
                            <c:forEach items="${websiteImagesList}" var="websiteImages" varStatus="index">
                                <li>
                                        <%--<input type="hidden" name="WebsiteImagesList[${index.index}].imageId" value="${websiteImages.imageId}">--%>
                                    <input type="hidden" name="WebsiteImagesList[${index.index}].imagesUrl" id="imagesUrlInput${index.index}" value="${websiteImages.imagesUrl}">
                                    <input type="hidden" name="WebsiteImagesList[${index.index}].previewUrl" value="${websiteImages.previewUrl}">
                                    <input type="hidden" name="WebsiteImagesList[${index.index}].webpageTemplateId" value="${websiteImages.webpageTemplateId}">
                                    <table class="mk-bj-list"  width="50%">
                                        <tr <c:if test="${upload_image=='false'}">style="display: none"</c:if>>
                                            <td style="text-align: left;"  colspan="2">
                                                <span class="tit-name"><font color="red">*</font>图片：</span>
                                                <c:choose>
                                                    <c:when test="${websiteImages.imagesUrl==null || websiteImages.imagesUrl==''}">
                                                        <img id="imagesUrl${index.index}" class="mk-list-img" src="${ctx }/static/admin/assets/logo.png">
                                                    </c:when>
                                                    <c:otherwise>
                                                        <img id="imagesUrl${index.index}" class="mk-list-img" src="<%=contextPath%>${websiteImages.imagesUrl}">
                                                    </c:otherwise>
                                                </c:choose>
                                                <input type="button" value="上传" id="imageFile${index.index}" class="imageFileUpload"/>
                                                <font color="red">(${upload_image_info})</font>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: left;<c:if test="${show_title=='false'}">display: none</c:if>" width="50%">
                                                <span class="tit-name"><font color="red">*</font>标题：</span>
                                                <input  name="WebsiteImagesList[${index.index}].title" type="text" value="${websiteImages.title}" class="txt" />
                                            </td>
                                            <td style="text-align: left;<c:if test="${websiteImagesList.size()==1 and add_new_image=='false'}">display: none</c:if>" width="50%">
                                                <span class="tit-name"> <font color="red">&nbsp;</font>排序：</span>
                                                <input id="sort" name="WebsiteImagesList[${index.index}].seriesNumber" data-rule="required;" class="txt hiddenSort" value="${websiteImages.seriesNumber }" onkeyup="this.value=this.value.replace(/\D/g,'')" type="hidden">
                                                <a class="acd-ico-btn a-i-b-up" href="javascript:void(0)" onclick="moveUp(this)" title="向上一级"  >&nbsp;</a>
                                                <a class="acd-ico-btn a-i-b-dwon" href="javascript:void(0)" onclick="moveDown(this)" title="向下一级"  >&nbsp;</a>
                                                <a class="acd-ico-btn a-i-b-close" href="javascript:void(0)" onclick="$(this).parents('li').remove()" title="删除" <c:if test="${delete_data=='false'}">style="display: none"</c:if> >&nbsp;</a>
                                            </td>
                                        </tr>
                                        <tr >
                                            <td <c:if test="${show_intruduction=='false'}">style="display: none"</c:if> style="text-align: left;">
                                                <span class="tit-name"> <font color="red">*</font>简介：</span>
                                                <input type="text" id="intruduction${index.index}"  class="txt"/>
                                            </td>
                                            <td <c:if test="${show_image_link=='false'}">style="display: none"</c:if>  style="text-align: left;">
                                                <span class="tit-name"><font color="red">&nbsp;</font>链接：</span>
                                                <input name="WebsiteImagesList[${index.index}].linkAddress" type="text" value="${websiteImages.linkAddress}" class="txt" />
                                            </td>
                                        </tr>
                                        <tr  >
                                            <td <c:if test="${show_info=='false'}">style="display: none"</c:if> style="text-align: left;">
                                                <span class="tit-name"> <font color="red">*</font>描述：</span>
                                                <textarea id="describe${index.index}" name="WebsiteImagesList[${index.index}].describe"  class="txt">${websiteImages.describe }</textarea>
                                            </td>
                                        </tr>

                                        <c:if test="${show_multi_data_remark>0}">
                                            <c:set value="${fn:split(websiteImages.describe,'|')}" var="describeArr" />
                                                <c:forEach begin="1" end="${show_multi_data_remark}" varStatus="indexRemark">
                                                    <tr>
                                                        <td style="text-align: left;">
                                                            <span class="tit-name"> <font color="red">*</font>描述${indexRemark.index}：</span>
                                                            <textarea name="show_multi_data_remark-textarea" describeIndex="${index.index}" class="txt">${describeArr[indexRemark.index-1]}</textarea>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                        </c:if>

                                    </table>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
            </c:if>
            <c:if test="${empty websiteImagesList && dynamic_data=='false'}">
                <div class="mt30">
                    <p class="hLh30 fsize20 c-333 f-fH">新增模块内容
                        <tt class=" ml20 fsize16">
                            （<font color="red">*</font>&nbsp;为必填项）
                        </tt>
                        <span class="field_desc"></span>
                    </p>
                    <ul class="mk-bj-li" id="templateData">

                    </ul>
                </div>
            </c:if>
            <c:if test="${dynamic_data=='true' && empty redirectIframeUrl}">
                <div class="">
                    <p class="hLh30 fsize20 c-333 f-fH">模块内容为调用接口的动态数据
                    </p>
                </div>
            </c:if>
            <c:if test="${dynamic_data=='true' && not empty redirectIframeUrl}">
                <div class="">
                    <p class="hLh30 fsize20 c-333 f-fH">模板内容
                    </p>
                    <c:if test="${not empty redirectIframeUrl}">
                        <ul class="mk-bj-li">
                            <iframe id="indexFrame" name="indexFrame" src="${redirectIframeUrl}" style="overflow:visible;visibility: hidden;" scrolling="yes" frameborder="no" width="100%" height=""></iframe>
                        </ul>
                    </c:if>
                </div>
            </c:if>

            <li>
                <div>
                    <input onclick="addImage()" class="button mr30 ml50" type="button" value="新增" <c:if test="${add_new_image=='false' || dynamic_data=='true'}">style="display: none"</c:if> >
                    <%--<input onclick="saveImage()" class="button mr30" type="button" value="修改">
                    <input onclick="window.parent.previewPageTemplate()" class="button button-sc" type="button" value="返回">--%>
                </div>
            </li>
        </form>
    </div>
<script>

    var K = window.KindEditor;//编辑器全局变量

    $(function() {
        //初始化图片上传
        $("input[id^=imageFile]").each(function (index, tag) {
            initSimpleImageUpload("imageFile" + index, "template_data_image", imgCallback, "", index);
        });
    });

    //图片上传回调
    function imgCallback(imgUrl,idIndex){
        $("#imagesUrlInput"+idIndex).val(imgUrl);
        $("#imagesUrl"+idIndex).attr('src',imagesPath+imgUrl);
    }

    /**
     * 初始化图片上传
     * @param btnId 上传组件的ID
     * @param param 图片上传目录名
     * @param callback 上传成功后的回调函数，函数接收一个参数（上传图片的URL）
     * @param pressText 是否上水印 false或空 否 true是
     */
    function initSimpleImageUpload(btnId,param,callback,pressText,idIndex){
        //KindEditor.ready(function(K) {
            var uploadbutton = K.uploadbutton({
                button : K('#'+btnId+'')[0],
                fieldName : "uploadfile",
                url : uploadSimpleUrl+'&param='+param+'&fileType=jpg,gif,png,jpeg&pressText='+pressText,
                afterUpload : function(data) {
                    if (data.error == 0) {
                        var url = K.formatUrl(data.url, 'absolute');//absolute,domain
                        callback(url,idIndex);
                    } else {
                        msgshow(data.message);
                    }
                },
                afterError : function(str) {
                    msgshow('自定义错误信息: ' + str);
                }
            });
            uploadbutton.fileBox.change(function(e) {
                uploadbutton.submit();
            });
        //});
    }

    //上移操作
    function moveUp(obj){
        var current_sort= $(obj).parents("li").find(".hiddenSort").val();

        var other_sort= $(obj).parents("li").prev().find(".hiddenSort").val();

        if(other_sort!=undefined){
            $(obj).parents("li").find(".hiddenSort").val(other_sort);
            $(obj).parents("li").prev().find(".hiddenSort").val(current_sort);

            $(obj).parents("li").insertBefore($(obj).parents("li").prev());    //移动节点
        }
    }

    //下移操作
    function moveDown(obj){
        var current_sort= $(obj).parents("li").find(".hiddenSort").val();

        var other_sort= $(obj).parents("li").next().find(".hiddenSort").val();

        if(other_sort!=undefined){
            $(obj).parents("li").find(".hiddenSort").val(other_sort);
            $(obj).parents("li").next().find(".hiddenSort").val(current_sort);

            $(obj).parents("li").next().insertBefore($(obj).parents("li"));    //移动节点
        }
    }

    /**
     * 左侧 返回按钮 点击 indexIframe 跳转
     */
    function leftClickJumpIframeUrl(){
        window.parent.updIndexFrame("${redirectIframeUrl}");
    }

    /**
     * 隐藏 模板 内容介绍
     */
    function hideTmplateInfo(){
        $("#tmplateInfo").hide();
    }

    /**
     * 显示 模板 内容介绍
     */
    function showTmplateInfo(){
        $("#tmplateInfo").show();
    }
</script>
</body>
</html>


