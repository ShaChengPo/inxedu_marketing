<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/base.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" http-equiv="Content-Type" />
<title>后台管理系统 - 因酷在线教育软件 - 在线教育整体解决方案提供商</title>
<link rel="stylesheet" type="text/css" href="${ctx}/static/admin/css/jquery.lightbox.css">
<%--<link rel="stylesheet" type="text/css" href="${ctx}/static/common/jquery.bigcolorpicker.css">--%>
<script type="text/javascript" src="${ctximg}/static/common/jquery.lightbox.min.js"></script>
<%--<script type="text/javascript" src="${ctximg}/static/common/tem.js"></script>--%>
<%--<script type="text/javascript" src="${ctximg}/static/common/jquery.bigcolorpicker.js"></script>--%>
<script type="text/javascript" src="${ctximg}/static/admin/js/highChart/highcharts.js"></script>
<script type="text/javascript" src="${ctximg}/static/admin/js/highChart/highcharts-3d.js"></script>
<script type="text/javascript" language="javascript">
    $(function () {
        $("#indexFrame").css("visibility","hidden");
        $("#indexFrame").attr("src","${webPageUrl}");

        var color = '${color}';
        if (color=="#dd491f"||color=="#68cb9b"||color=="#009ed9"){
            $(color).prop("checked", true)
        }else {
            $("#imageColor").prop("checked", true);
            $("#custom").css("background",color);
        }
    });

    /**
     * 设置iframe 高度
     */
    function setIframeHeight(iframe) {
        if (iframe) {
            var iframeWin = iframe.contentWindow || iframe.contentDocument.parentWindow;
            if (iframeWin.document.body) {
                iframe.height = iframeWin.document.documentElement.scrollHeight+10 || iframeWin.document.body.scrollHeight+10;
                $("#indexFrame").css("visibility","visible");
            }
        }
    }
    window.onload = function () {
        setIframeHeight(document.getElementById('indexFrame'));
    };

    /*使用模板 设为首页*/
function setHelloPage(webpageId) {
    $.ajax({
        url:'/admin/ajax/setHelloPage/'+webpageId,
        type:'post',
        dataType:'json',
        success:function(result){
            if(result.success==true){
                msgshow(result.message);
                //模板 是否 修改 過(排序 ，刪除 ，新增)
                window.parent.isDataChange=false;//還原后默認false
                //重新加载 左側菜單
                window.parent.getWebpageTemplates(result.entity.publishUrl);
                //重新加载
                window.location.reload();
            }else{
                msgshow(result.message,false);
            }
        },
        error:function(error){
            msgshow("系统繁忙，请稍后再操作！",'false');
        }
    });
}
/*更换模板颜色*/
function updColor() {
    $("#updColor").submit()
}
    function publishWebpage() {
        if(!confirm('是否发布?')){
            return;
        }
        window.parent.addTemplate();
    }
    /**
     * 还原
     */
    function returnWebpage(id,em){
        if(!confirm('确实要还原吗?')){
            return;
        }
        $.ajax({
            url:'/admin/ajax/webpageReturn/'+id,
            type:'post',
            dataType:'json',
            success:function(result){
                if(result.success==true){
                    msgshow("还原成功");

                    //模板 是否 修改 過(排序 ，刪除 ，新增)
                    window.parent.isDataChange=false;//還原后默認false
                    //重新加载 左側菜單
                    window.parent.getWebpageTemplates(result.entity.publishUrl);
                    //重新加载
                    window.location.reload();

                }else{
                    msgshow(result.message,'false');
                }
            },
            error:function(error){
                msgshow("系统繁忙，请稍后再操作！",'false');
            }
        });
    }
$(function () {
    var webpageId =  '${webpage.id}';
    $("#"+webpageId).addClass("current");
})
</script>
	<%--通过首页传参数判断是否弹出登陆窗口--%>
</head>
<body style="background: none;">
	<div class="rMain">
        <%--<c:if test="${webPageUrl=='/index.html'}">
            <div class="ts-box">
                <p class="fsize14 c-red f-fM ml30">
                    <span class="fsize16">提示：</span>平台提供以下模板，也可使用编辑器自定义模块，
                    功能结构模块效果图预览只供参考，最终页面呈现以生成首页web页面为主！
                </p>
            </div>
        </c:if>--%>
        <%--<div>
            <c:if test="${webPageUrl=='/index.html'}">
                <p class="hLh30 fsize20 c-333 f-fH mt30">首页模板</p>
                <div class="mb-wrap mt20">
                    <ul class="clearfix mb-list">
                        <li id="7">
                            <div class="nr-box">
                                <div class="pic">
                                    <a href="/static/admin/mb-pic/mb-1.jpg" rel="lightbox[plants]" title="首页模板一" class="lightbox-enabled">
                                        <img src="/static/admin/mb-pic/mb-1.jpg">
                                    </a>
                                </div>
                                <div class="mt10 ml10 mr10">
                                    <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">首页模板一</h5>
                                  &lt;%&ndash;  <p class="txt-nr">
                                        焦点图轮播、资讯、教师列表等功能结构。焦点图片规格：1920px*600px，（建议图片大小150kb以内！）不同尺寸PC设备下都展示通栏效果
                                    </p>&ndash;%&gt;
                                </div>
                            </div>
                            <div class="tac mt20">
                                <a href="javascript:void(0)" onclick="setHelloPage(7)" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模板</a>
                            </div>
                        </li>
                        <li id="47">
                            <div class="nr-box">
                                <div class="pic">
                                    <a href="/static/admin/mb-pic/mb-1.jpg" rel="lightbox[plants]" title="首页模板二" class="lightbox-enabled">
                                        <img src="/static/admin/mb-pic/mb-2.jpg">
                                    </a>
                                </div>
                                <div class="mt10 ml10 mr10">
                                    <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">首页模板二</h5>
                                   &lt;%&ndash; <p class="txt-nr">
                                        焦点图轮播、各平台功能特点介绍等。焦点图片规格：1920px*720px，（建议图片大小150kb以内！）不同尺寸PC设备下都展示通栏效果
                                    </p>&ndash;%&gt;
                                </div>
                            </div>
                            <div class="tac mt20">
                                <a href="javascript:void(0)" onclick="setHelloPage(47)" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模板</a>
                            </div>
                        </li>
                        <li id="48">
                            <div class="nr-box">
                                <div class="pic">
                                    <a href="/static/admin/mb-pic/mb-1.jpg" rel="lightbox[plants]" title="首页模板三" class="lightbox-enabled">
                                        <img src="/static/admin/mb-pic/mb-3.jpg">
                                    </a>
                                </div>
                                <div class="mt10 ml10 mr10">
                                    <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">首页模板三</h5>
                                   &lt;%&ndash; <p class="txt-nr">
                                        焦点图轮播、资讯、教师列表等功能结构。焦点图片规格：1920px*600px，（建议图片大小150kb以内！）不同尺寸PC设备下都展示通栏效果
                                    </p>&ndash;%&gt;
                                </div>
                            </div>
                            <div class="tac mt20">
                                <a href="javascript:void(0)" onclick="setHelloPage(48)" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模板</a>
                            </div>
                        </li>
                        <li id="49">
                            <div class="nr-box">
                                <div class="pic">
                                    <a href="/static/admin/mb-pic/mb-1.jpg" rel="lightbox[plants]" title="首页模板四" class="lightbox-enabled">
                                        <img src="/static/admin/mb-pic/mb-4.jpg">
                                    </a>
                                </div>
                                <div class="mt10 ml10 mr10">
                                    <h5 class="unFw fsize14 c-333 tac txtOf hLh30 f-fM">首页模板四</h5>
                                    &lt;%&ndash;<p class="txt-nr">
                                        焦点图轮播、资讯、教师列表等功能结构。焦点图片规格：1920px*720px，（建议图片大小150kb以内！）不同尺寸PC设备下都展示通栏效果
                                    </p>&ndash;%&gt;
                                </div>
                            </div>
                            <div class="tac mt20">
                                <a href="javascript:void(0)" onclick="setHelloPage(49)" class="master-btn c-fff fsize16 f-fM tac btn btn-big">使用模板</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <p class="hLh30 fsize20 c-333 f-fH mt30">一键换肤</p>
                <div class="color-warp mt20 clearfix">
                    <form action="${ctx}/admin/main/updColor" id="updColor" method="post">
                    <div class="colo-box fl">
                        <label  onclick="updColor()" title="官方橙">
                            <input id="dd491f" name="color" value="#dd491f" type="radio">
                            <span style="background: #dd491f;">官方橙</span>
                        </label>
                        <label  onclick="updColor()"  title="官方绿">
                            <input  id="68cb9b" name="color" value="#68cb9b" type="radio">
                            <span style="background: #68cb9b;">官方绿</span>
                        </label>
                        <label  onclick="updColor()"  title="官方蓝">
                            <input id="009ed9" name="color" value="#009ed9" type="radio">
                            <span style="background: #009ed9;">官方蓝</span>
                        </label>
                    </div>
                    <div class="colo-box fl">
                        <label title="自定义">
                            <input id="imageColor" name="color" value="#ccc"  type="radio">
                            <span id="custom" style="background: #ccc;">自定义</span>
                        </label>
                    </div>
                    </form>
                    <div class="clear"></div>
                    <p class="hLh30 fsize14 c-red f-fM mt20 pb30">
                        (注释：一键换肤功能，可改变整个平台主色调，用户可根据自己平台选择合适的主色调。)
                    </p>
                </div>
            </c:if>
            <div class="mb30 clearfix">
                <div class="fl">
                    <p class="hLh30 fsize20 c-333 f-fH">
                        页面展示
                    </p>
                </div>
               <div class="fl ml30">
                   <a href="javascript:void(0)" onclick="publishWebpage(${webpage.id})" title="生成" class="master-btn btn btn-smal">发布</a>
                   <a href="javascript:void(0)" onclick="returnWebpage(${webpage.id})" title="还原" class="gry-btn btn btn-smal">还原</a>
               </div>
            </div>--%>
            <div class="html-box mt20">
                <iframe id="indexFrame" name="indexFrame" src="" style="overflow:visible;visibility: hidden;" scrolling="yes" frameborder="no" width="100%" height=""></iframe>
            </div>
        </div>
	<%--</div>--%>
<script>
    $(function () {
        $("#bigpicker").click(function () {
            $("#updColor").submit()
        })
    })
</script>
</body>
</html>