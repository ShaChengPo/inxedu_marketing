package com.inxedu.os.edu.entity.templatedatasource;

import lombok.Data;

import java.io.Serializable;

/**
 * @author www.inxedu.com
 * @description 模板动态数据来源
 */
@Data
public class TemplateDatasource implements Serializable {
    private static final long serialVersionUID = 3148176768559230877L;

	/** 数据key */
	private String dataKey;
	/** 获取数据接口路径 */
	private String dataUrl;
	/** 描述 */
	private String describe;
	/** 请求地址类型 1,网校 2，社区 */
	private int urlType;

}

