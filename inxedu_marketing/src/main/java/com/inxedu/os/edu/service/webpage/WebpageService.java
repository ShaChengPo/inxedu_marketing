package com.inxedu.os.edu.service.webpage;

import com.inxedu.os.common.entity.PageEntity;
import com.inxedu.os.edu.entity.webpage.Webpage;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author www.inxedu.com
 * @description 页面 WebpageService接口
 */
public interface WebpageService{
	/**
     * 添加页面
     */
    Long addWebpage(Webpage webpage);
    
    /**
     * 删除页面
     * @param id
     */
    void delWebpageById(Long id);
    
    /**
     * 修改页面
     * @param webpage
     */
    void updateWebpage(Webpage webpage);
    
    /**
     * 通过id，查询页面
     * @param id
     * @return
     */
    Webpage getWebpageById(Long id);
    
    /**
     * 分页查询页面列表
     * @param webpage 查询条件
     * @param page 分页条件
     * @return List<Webpage>
     */
    List<Webpage> queryWebpageListPage(Webpage webpage, PageEntity page);
    
    /**
     * 条件查询页面列表
     * @param webpage 查询条件
     * @return List<Webpage>
     */
    List<Webpage> queryWebpageList(Webpage webpage);

    /**
     * 发布页面
     * @param id
     * @return
     */
    Map<String,String> updWebpagePublish(Long id, String rootPath)throws Exception;

    /**
     * 全站发布
     * isPublishAll 是否发布所有 true 发布所有 ，false 发布 除了首页外的 页面
     */
    void publishAll(boolean isPublishAll,HttpServletRequest request);

    /**
     * 模板 root 数据
     * @return
     */
    Map<String, Object> getTemplateRootMap(Webpage webpage);
}



