package com.inxedu.os.edu.dao.impl.webpagetemplate;

import com.inxedu.os.common.dao.GenericDaoImpl;
import com.inxedu.os.common.entity.PageEntity;
import com.inxedu.os.edu.dao.webpagetemplate.WebpageTemplateDao;
import com.inxedu.os.edu.entity.webpagetemplate.WebpageTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author www.inxedu.com
 * @description 页面和模板中间表 WebpageTemplateDao接口实现
 */
@Repository("webpageTemplateDao")
public class WebpageTemplateDaoImpl extends GenericDaoImpl implements WebpageTemplateDao{
	/**
     * 添加页面和模板中间表
     */
    public Long addWebpageTemplate(WebpageTemplate webpageTemplate){
    	this.insert("WebpageTemplateMapper.addWebpageTemplate", webpageTemplate);
		return webpageTemplate.getId();
    }
    
    /**
     * 删除页面和模板中间表
     * @param id
     */
    public void delWebpageTemplateById(Long id){
    	this.update("WebpageTemplateMapper.delWebpageTemplateById", id);
    }
    
    /**
     * 修改页面和模板中间表
     * @param webpageTemplate
     */
    public void updateWebpageTemplate(WebpageTemplate webpageTemplate){
    	this.update("WebpageTemplateMapper.updateWebpageTemplate", webpageTemplate);
    }
    
    /**
     * 通过id，查询页面和模板中间表
     * @param id
     * @return
     */
    public WebpageTemplate getWebpageTemplateById(Long id){
    	return this.selectOne("WebpageTemplateMapper.getWebpageTemplateById", id);
    }
    
    /**
     * 分页查询页面和模板中间表列表
     * @param webpageTemplate 查询条件
     * @param page 分页条件
     * @return List<WebpageTemplate>
     */
    public List<WebpageTemplate> queryWebpageTemplateListPage(WebpageTemplate webpageTemplate,PageEntity page){
    	return this.queryForListPage("WebpageTemplateMapper.queryWebpageTemplateListPage", webpageTemplate, page);
    }
    
    /**
     * 条件查询页面和模板中间表列表
     * @param webpageTemplate 查询条件
     * @return List<WebpageTemplate>
     */
    public List<WebpageTemplate> queryWebpageTemplateList(WebpageTemplate webpageTemplate){
    	return this.selectList("WebpageTemplateMapper.queryWebpageTemplateList", webpageTemplate);
    }

    @Override
    public void delWebpageTemplateByWebpageId(Long webpageId) {
        this.delete("WebpageTemplateMapper.delWebpageTemplateByWebpageId", webpageId);
    }

    @Override
    public void delWebpageTemplate(WebpageTemplate webpageTemplate) {
        this.delete("WebpageTemplateMapper.delWebpageTemplate", webpageTemplate);
    }
}



