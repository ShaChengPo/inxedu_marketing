package com.inxedu.os.edu.controller.website;


import com.inxedu.os.common.controller.BaseController;
import com.inxedu.os.common.sysLog.SystemLog;
import com.inxedu.os.common.util.WebUtils;
import com.inxedu.os.edu.entity.webpage.Webpage;
import com.inxedu.os.edu.entity.webpagetemplate.WebpageTemplate;
import com.inxedu.os.edu.entity.website.WebsiteNavigate;
import com.inxedu.os.edu.service.webpage.WebpageService;
import com.inxedu.os.edu.service.webpagetemplate.WebpageTemplateService;
import com.inxedu.os.edu.service.website.WebsiteImagesService;
import com.inxedu.os.edu.service.website.WebsiteNavigateService;
import com.inxedu.os.edu.service.website.WebsiteProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 
 * 导航管理
 * @author www.inxedu.com
 */
@Controller
@RequestMapping("/admin")
public class AdminWebsiteNavigateController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(AdminWebsiteNavigateController.class);
 	@Autowired
    private WebsiteNavigateService websiteNavigateService;
	@Autowired
	private WebpageService webpageService;
	@Autowired
	private WebsiteProfileService websiteProfileService;
	@Autowired
	private WebsiteImagesService websiteImagesService;
	@Autowired
	private WebpageTemplateService webpageTemplateService;
 	private static final String getWebsiteNavigateList = getViewPath("/admin/website/navigate/websiteNavigate_list");//导航列表
	private static final String updateWebsiteNavigate = getViewPath("/admin/website/navigate/websiteNavigate_update");//修改 导航配置
	private static final String addWebsiteNavigate = getViewPath("/admin/website/navigate/websiteNavigate_add");//跳转添加导航
	// 创建群 绑定变量名字和属性，把参数封装到类
	@InitBinder("websiteNavigate")
	public void initBinderWebsiteNavigate(WebDataBinder binder) {
		binder.setFieldDefaultPrefix("websiteNavigate.");
	}
 	/**
	 * 查询导航配置
	 */
	@RequestMapping("/website/navigates")
	public ModelAndView showWebsiteNavigates(HttpServletRequest request,WebsiteNavigate websiteNavigate,String type){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(getWebsiteNavigateList);
		try{
			websiteNavigate.setType(type);
			List<WebsiteNavigate> websiteNavigates=websiteNavigateService.getWebsiteNavigate(websiteNavigate);
			modelAndView.addObject("websiteNavigates",websiteNavigates);
			request.getSession().setAttribute("websiteListUri", WebUtils.getServletRequestUriParms(request));
			modelAndView.addObject("type",type);
		}catch(Exception e){
			logger.error("AdminWebsiteNavigateController.showWebsiteNavigates--导航列表出错", e);
			return new ModelAndView(setExceptionRequest(request, e));
		}
		return modelAndView;
	}
	/**
	 * 跳转添加导航
	 */
	@RequestMapping("/website/doAddNavigates")
	public ModelAndView doAddWebsiteNavigate(HttpServletRequest request){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(addWebsiteNavigate);
		return modelAndView;
	}
	/**
	 * 添加导航配置
	 */
	@RequestMapping("/website/addNavigate")
	@SystemLog(type="add",operation="添加导航")
	@ResponseBody
	public Map<String,Object> addWebsiteNavigate(WebsiteNavigate websiteNavigate,HttpServletRequest request){
		Map<String, Object> json = new HashMap<String, Object>(4);
		try{
			if(websiteNavigate!=null && websiteNavigate.getName()!=null && websiteNavigate.getName().trim().length()>0){
					/*url查重*/
				List<WebsiteNavigate> websiteNavigateList = websiteNavigateService.getWebsiteNavigate(new WebsiteNavigate());
				Webpage webpage = new Webpage();
				if("INDEX".equals(websiteNavigate.getType())){

					/*获取所有导航的url*/
					List urlList = new ArrayList();
					for (WebsiteNavigate websiteNavigate1:websiteNavigateList){
						urlList.add(websiteNavigate1.getUrl());
					}
					if (urlList.contains(websiteNavigate.getUrl())||websiteNavigate.getUrl().equals("/index.html")){
						json = this.setJson(false,"链接已存在!",null);
						return json;
					}
					/*创建页面*/

					webpage.setPageName(websiteNavigate.getName());
					webpage.setPublishUrl(websiteNavigate.getUrl());
					webpage.setTitle(websiteNavigate.getName());
					webpage.setCreateTime(new Date());
					webpageService.addWebpage(webpage);
					websiteNavigate.setPageId(webpage.getId());
				}

				/*给新建赋排序值 最大排序值+1*/
                int newOrderNum = websiteNavigateList.get(websiteNavigateList.size()-1).getOrderNum()+1;
                websiteNavigate.setOrderNum(newOrderNum);
				websiteNavigateService.addWebsiteNavigate(websiteNavigate);
				if("INDEX".equals(websiteNavigate.getType())){
					/*给页面创建头尾模板*/
					WebpageTemplate webpageTemplateHeader = new WebpageTemplate();
					WebpageTemplate webpageTemplateFooter = new WebpageTemplate();
					webpageTemplateHeader.setPageId(webpage.getId());
					webpageTemplateHeader.setTemplateName("公共头部");
					webpageTemplateHeader.setTemplateTitle("公共头部");
					webpageTemplateHeader.setEffective(1);
					webpageTemplateHeader.setSort(1);
					webpageTemplateHeader.setTemplateUrl("/template/templet1/公共头部.html");
					webpageTemplateFooter.setPageId(webpage.getId());
					webpageTemplateFooter.setTemplateName("公共尾部");
					webpageTemplateFooter.setTemplateTitle("公共尾部");
					webpageTemplateFooter.setSort(2);
					webpageTemplateFooter.setEffective(1);
					webpageTemplateFooter.setTemplateUrl("/template/templet1/公共尾部.html");
					webpageTemplateService.addWebpageTemplate(webpageTemplateHeader);
					webpageTemplateService.addWebpageTemplate(webpageTemplateFooter);
					webpageService.updWebpagePublish(webpage.getId(),request.getSession().getServletContext().getRealPath("/"));
				}
				webpageService.publishAll(true,request);
			}

			/*Object uri = request.getSession().getAttribute("websiteListUri");
			if(uri!=null){
				return "redirect:"+uri.toString();
			}*/
			json = this.setJson(true,"添加成功!",websiteNavigate);
		}catch(Exception e){
			logger.error("AdminWebsiteNavigateController.addWebsiteNavigate--添加导航出错", e);
			json = setJson(false, "更新导航出错", null);
		}
		return json;
	}
	
	/**
	 * 跳转更新导航配置
	 */
	@RequestMapping("/website/doUpdateNavigate/{id}")
	public ModelAndView doUpdateWebsiteNavigate(@PathVariable Integer id,HttpServletRequest request){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(updateWebsiteNavigate);
		try{
			if(id!=null){
				WebsiteNavigate websiteNavigate=websiteNavigateService.getWebsiteNavigateById(id);
				modelAndView.addObject("websiteNavigate",websiteNavigate);
				modelAndView.addObject("url",websiteNavigate.getUrl());
				webpageService.publishAll(true,request);
			}
		}catch(Exception e){
			logger.error("AdminWebsiteNavigateController.doUpdateWebsiteNavigates--跳转更新导航出错", e);
			return new ModelAndView(setExceptionRequest(request, e));
		}
		return modelAndView;
	}
	/**
	 * 更新导航配置
	 */
	@RequestMapping("/website/updateNavigate")
	@SystemLog(type="update",operation="更新导航")
	@ResponseBody
	public  Map<String,Object> updatewebsiteNavigate(WebsiteNavigate websiteNavigate,HttpServletRequest request){
		Map<String, Object> json = new HashMap<String, Object>(4);
		try{

			if(websiteNavigate!=null && websiteNavigate.getName()!=null && websiteNavigate.getName().trim().length()>0){
				WebsiteNavigate oldWebsiteNavigate = websiteNavigateService.getWebsiteNavigateById(websiteNavigate.getId());
				websiteNavigate.setPageId(oldWebsiteNavigate.getPageId());
				if (!oldWebsiteNavigate.getUrl().equals(websiteNavigate.getUrl())){
					/*url查重*/
					List<WebsiteNavigate> websiteNavigateList = websiteNavigateService.getWebsiteNavigate(new WebsiteNavigate());
					/*获取所有导航的url*/
					List urlList = new ArrayList();
					for (WebsiteNavigate websiteNavigate1:websiteNavigateList){
						urlList.add(websiteNavigate1.getUrl());
					}
					if (urlList.contains(websiteNavigate.getUrl())||websiteNavigate.getUrl().equals("/index.html")){
						json = this.setJson(false,"链接已存在!",null);
						return json;
					}
				}

				websiteNavigateService.updateWebsiteNavigate(websiteNavigate);
				if ("INDEX".equals(websiteNavigate.getType())){
					Webpage webpage = webpageService.getWebpageById(websiteNavigate.getPageId());
					webpage.setTitle(websiteNavigate.getName());
					webpage.setPageName(websiteNavigate.getName());
					if (websiteNavigate.getUrl().equals("/")){
						webpage.setPublishUrl("/index.html");
					}else {
						webpage.setPublishUrl(websiteNavigate.getUrl());
					}
					webpageService.updateWebpage(webpage);
				}

			}
			/*Object uri = request.getSession().getAttribute("websiteListUri");
			if(uri!=null){
				return "redirect:"+uri.toString();
			}*/


			json = this.setJson(true,"更新成功！",websiteNavigate);
		}catch(Exception e){
			logger.error("AdminWebsiteNavigateController.updatewebsiteNavigate--更新导航出错", e);
			json = setJson(false, "更新导航出错", null);
		}
		return json;
	}
	/**
	 * 冻结或解冻导航
	 */
	@RequestMapping("/website/freezeNavigate")
	@ResponseBody
	@SystemLog(type="update",operation="冻结或解冻导航")
	public Map<String,Object> freezeWebsiteNavigate(WebsiteNavigate websiteNavigate,HttpServletRequest request){
		Map<String, Object> json = new HashMap<String, Object>(4);
		try{
			websiteNavigateService.freezeWebsiteNavigate(websiteNavigate);
			webpageService.publishAll(true,request);
			json = setJson(true, "true", null);
		}catch(Exception e){
			logger.error("AdminWebsiteNavigateController.freezeWebsiteNavigate--更新导航出错", e);
			json = setJson(false, "false", null);
		}
		return json;
	}
	/**
	 * 删除导航
	 */
	@RequestMapping("/website/delNavigate/{id}")
	@ResponseBody
	@SystemLog(type="del",operation="删除导航")
	public Map<String,Object> delWebsiteNavigate(HttpServletRequest request,@PathVariable Integer id){
		Map<String, Object> json = new HashMap<String, Object>(4);
		try{
			WebsiteNavigate websiteNavigate = websiteNavigateService.getWebsiteNavigateById(id);
			webpageService.delWebpageById(websiteNavigate.getPageId());
			websiteImagesService.delImagesByPageId(websiteNavigate.getPageId());
			webpageTemplateService.delWebpageTemplateByWebpageId(websiteNavigate.getPageId());
			websiteNavigateService.delWebsiteNavigate(id);
			webpageService.publishAll(true,request);
			json = setJson(true, "true", null);
		}catch(Exception e){
			logger.error("AdminWebsiteNavigateController.delWebsiteNavigate--删除导航出错", e);
			json = setJson(false, "false", null);
		}
		return json;
	}
	/**
	 * 上移导航
	 */
	@RequestMapping("/website/moveNavUp")
	@ResponseBody
	@SystemLog(type="moveNavUp",operation="上移导航")
	public Map<String,Object> moveNavUp(HttpServletRequest request){
		Map<String, Object> json = new HashMap<String, Object>(4);
		try{
			int navId = Integer.parseInt(request.getParameter("navId"));
			int orderNum = Integer.parseInt(request.getParameter("orderNum"));
			List<WebsiteNavigate> websiteNavigateList = websiteNavigateService.getWebsiteNavigate(new WebsiteNavigate());
			for (int i=0;i<websiteNavigateList.size();i++){
				if (websiteNavigateList.get(i).getId()==navId){
					websiteNavigateList.get(i-1).setOrderNum(orderNum);
					websiteNavigateList.get(i).setOrderNum(orderNum-1);
					websiteNavigateService.updateWebsiteNavigate(websiteNavigateList.get(i));
					websiteNavigateService.updateWebsiteNavigate(websiteNavigateList.get(i-1));
					orderNum = websiteNavigateList.get(i).getOrderNum();
				}
			}
			json = setJson(true, orderNum+"", null);
		}catch(Exception e){
			logger.error("AdminWebsiteNavigateController.moveNavUp--上移出错", e);
			json = setJson(false, "false", null);
		}
		return json;
	}
	/**
	 * 下移导航
	 */
	@RequestMapping("/website/moveNavDown")
	@ResponseBody
	@SystemLog(type="moveNavDown",operation="下移导航")
	public Map<String,Object> moveNavDown(HttpServletRequest request){
		Map<String, Object> json = new HashMap<String, Object>(4);
		try{
			int navId = Integer.parseInt(request.getParameter("navId"));
			int orderNum = Integer.parseInt(request.getParameter("orderNum"));
			List<WebsiteNavigate> websiteNavigateList = websiteNavigateService.getWebsiteNavigate(new WebsiteNavigate());
			for (int i=0;i<websiteNavigateList.size();i++){
				if (websiteNavigateList.get(i).getId()==navId){
					websiteNavigateList.get(i+1).setOrderNum(orderNum);
					websiteNavigateList.get(i).setOrderNum(orderNum+1);
					websiteNavigateService.updateWebsiteNavigate(websiteNavigateList.get(i));
					websiteNavigateService.updateWebsiteNavigate(websiteNavigateList.get(i+1));
					orderNum = websiteNavigateList.get(i).getOrderNum();
				}
			}
			json = setJson(true,orderNum+"", null);
		}catch(Exception e){
			logger.error("AdminWebsiteNavigateController.moveNavDown--下移出错", e);
			json = setJson(false, "false", null);
		}
		return json;
	}
}