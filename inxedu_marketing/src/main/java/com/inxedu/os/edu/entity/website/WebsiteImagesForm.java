package com.inxedu.os.edu.entity.website;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * banner图管理 多个 list（form提交）
 * @author www.inxedu.com
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class WebsiteImagesForm implements Serializable{
    private List<WebsiteImages> WebsiteImagesList;
}
