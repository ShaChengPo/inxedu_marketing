package com.inxedu.os.edu.service.webpage;

import com.inxedu.os.common.util.ObjectUtils;
import com.inxedu.os.edu.entity.webpage.Webpage;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by lucky on 2016/9/29.
 */
public class WebPagePublishThread extends Thread{
    private WebpageService webpageService;
    private String rootPath;

    @Getter
    private static int listSum;
    @Getter
    private static int listNum;
    @Getter
    private static boolean publishIng=false;
    @Getter
    @Setter
    private boolean isPublishAll=true;
    public WebPagePublishThread() {
    }

    public WebPagePublishThread(WebpageService webpageService,String rootPath) {
        this.webpageService = webpageService;
        this.rootPath=rootPath;
    }


    @Override
    public void run() {
        try {
            Webpage webpage=new Webpage();
            if(isPublishAll==false){
                webpage.setNotPublishIndex(true);
            }
            List<Webpage> webpageList=webpageService.queryWebpageList(webpage);
            if(ObjectUtils.isNotNull(webpageList)){
                listSum=webpageList.size();
                publishIng=true;
                for (int i=0;i<webpageList.size();i++){
                    listNum=i+1;
                    webpageService.updWebpagePublish(webpageList.get(i).getId(),this.rootPath);
                    Thread.currentThread().sleep(30);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            listNum= listSum;
            publishIng=false;
        }

    }
}

